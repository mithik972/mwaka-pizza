<?php

namespace App\Tests\Entity;

use App\Entity\Ingredients;
use PHPUnit\Framework\TestCase;

class IngredientUnitTest extends TestCase
{
    /**
     * @return array
     */
    public function initIngredient () :array
    {
        $ingredient = new Ingredients();
        $datetime = new \DateTimeImmutable();

        $ingredient->setName('ingredient');
        $ingredient->setPrice(3.0);
        $ingredient->setCreatedAt($datetime);

        return [$ingredient,$datetime];
    }

    public function testIsTrue()  :void
    {
       list($ingredient, $datetime) = $this->initIngredient();

        $this->assertTrue($ingredient->getName() === 'ingredient');
        $this->assertTrue($ingredient->getPrice() === 3.0);
        $this->assertTrue($ingredient->getCreatedAt() === $datetime);
    }

    public function testIsFalse() :void
    {
        list($ingredient) = $this->initIngredient();

        $this->assertFalse($ingredient->getName() === 'new ingredient');
        $this->assertFalse($ingredient->getPrice() === 3.1);
        $this->assertFalse($ingredient->getCreatedAt() ===  new \DateTimeImmutable());
    }
}