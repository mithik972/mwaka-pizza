<?php

namespace App\Tests\Entity;

use App\Entity\Ingredients;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    /**
     * @return User
     */
    public function initUser() :User
    {
        $user = new User();
        $user->setEmail('user@mail.com');
        $user->setPassword('1234');
        $user->setRoles(['ROlE_USER']);

        return $user;
    }

    public function testIsTrue() :void
    {
        $user = $this->initUser();

        $this->assertTrue($user->getEmail() === 'user@mail.com');
        $this->assertTrue($user->getPassword() === '1234');
        $this->assertTrue($user->getRoles() === ['ROlE_USER']);
    }

    public function testAddGetRemoveAllergies()  :void
    {
        $user = new User();
        $ingredient = new Ingredients();

        $this->assertEmpty($user->getAllergies());

        $user->addAllergy($ingredient);
        $this->assertContains($ingredient, $user->getAllergies());

        $user->removeAllergy($ingredient);
        $this->assertEmpty($user->getAllergies());
    }
}