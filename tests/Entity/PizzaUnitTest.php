<?php


namespace App\Tests\Entity;


use App\Entity\Ingredients;
use App\Entity\Pizza;
use JetBrains\PhpStorm\Pure;
use phpDocumentor\Reflection\Types\Void_;
use PHPUnit\Framework\TestCase;

class PizzaUnitTest extends TestCase
{
    /**
     * @return array
     */
    private function initPizza() :array
    {
        $pizza = new Pizza();
        $datetime = new \DateTimeImmutable();

        $pizza->setName('pizza_name');
        $pizza->setImage('pizza.png');
        $pizza->setCreatedAt($datetime);

        return [$pizza,$datetime];
    }

    public function testIsTrue() :void
    {
        list($pizza,$datetime) = $this->initPizza();

        $this->assertTrue($pizza->getName() === 'pizza_name');
        $this->assertTrue($pizza->getImage() === 'pizza.png');
        $this->assertTrue($pizza->getCreatedAt() === $datetime);
    }

    public function testIsFalse() :void
    {
        list($pizza) = $this->initPizza();

        $this->assertFalse($pizza->getName() === 'name');
        $this->assertFalse($pizza->getImage() === 'image.png');
        $this->assertFalse($pizza->getCreatedAt() === new \DateTimeImmutable());

    }

    public function testAddGetRemoveIngredients()  :void
    {
        $pizza = new Pizza();
        $ingredient = new Ingredients();

        $this->assertEmpty($pizza->getIngredients());

        $pizza->addIngredient($ingredient);
        $this->assertContains($ingredient, $pizza->getIngredients());

        $pizza->removeIngredient($ingredient);
        $this->assertEmpty($pizza->getIngredients());

    }

    public function testCalculatedPizzaPrice()  :void
    {
        $pizza = new Pizza();
        $ingredient = new Ingredients();
        $ingredient1 = new Ingredients();

        $ingredient->setPrice(2.5);
        $ingredient1->setPrice(3.5);
        $price = $ingredient->getPrice() + $ingredient1->getPrice();
        $pizza->addIngredient($ingredient1);
        $pizza->addIngredient($ingredient);
        $pizza->calculatePrice();
        $this->assertTrue($pizza->getPrice() === $price);
    }
}