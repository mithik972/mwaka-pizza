<?php

namespace App\Entity;

use App\Repository\PizzaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Pizza
 * @package App\Entity
 * @author bernard SIMEON
 */
#[ORM\Entity(repositoryClass: PizzaRepository::class)]
class Pizza
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank]
    #[Assert\Length(min:2, max: 50)]
    private string $name;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image = null;

    #[ORM\Column]
    #[Assert\NotNull]
    private float $price = 0.00;

    #[ORM\ManyToMany(targetEntity: Ingredients::class)]
    private Collection $ingredients;

    #[ORM\Column]
    private \DateTimeImmutable $createdAt;

    /**
     * Pizza constructor.
     */
    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get Image
     *
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return $this
     */
    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get Price
     *
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * Set Price
     *
     * @param float $price
     *
     * @return $this
     */
    private function setPrice(float $price): self
    {
        $this->price = $price ;
        return $this;
    }

    /**
     * Get ingredients
     *
     * @return Collection<int, Ingredients>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    /**
     * add ingredient
     *
     * @param Ingredients $ingredient
     *
     * @return $this
     */
    public function addIngredient(Ingredients $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
        }

        $this->calculatePrice();

        return $this;
    }

    /**
     * remove ingredient
     *
     * @param Ingredients $ingredient
     *
     * @return $this
     */
    public function removeIngredient(Ingredients $ingredient): self
    {
        $this->ingredients->removeElement($ingredient);
        $this->calculatePrice();

        return $this;
    }

    /**
     * Get created at
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Set created at
     *
     * @param \DateTimeImmutable $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     *  Calculate price
     */
    public function calculatePrice(): void
    {
        $price = 0;
        foreach ($this->ingredients as $ingredient) {
            $price += $ingredient->getPrice();
        }

        $this->setPrice($price);
    }
}
