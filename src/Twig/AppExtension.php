<?php

namespace App\Twig;

use App\Entity\Ingredients;
use App\Entity\Pizza;
use App\Entity\User;
use JetBrains\PhpStorm\Pure;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class AppExtension
 * @package App\Twig
 * @author bernard SIMEON
 */
class AppExtension extends AbstractExtension
{
    /**
     * @inheritDoc
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
            new TwigFilter('isAnAllergicPizza', [$this, 'isAnAllergicPizza']),
            new TwigFilter('isAnAllergicIngredient', [$this, 'isAnAllergicIngredient'])
        ];
    }

    /**
     * Format Price
     *
     * @param $number
     *
     * @return string
     */
    #[Pure]
    public function formatPrice($number): string
    {
        return number_format($number,2);
    }

    /**
     * Is An Allergic Pizza
     *
     * @param Pizza $pizza
     * @param User $user
     *
     * @return bool
     */
    public function isAnAllergicPizza(Pizza $pizza, User $user): bool
    {
        $ingredients = $pizza->getIngredients()->toArray();
        $findAllergies = false;

        foreach ($user->getAllergies() as $allergy) {
            if (in_array($allergy, $ingredients)) {
                $findAllergies = true;
                break;
            }
        }

        return $findAllergies;
    }

    /**
     * Is An Allergic Ingredient
     *
     * @param Ingredients $ingredients
     * @param User $user
     *
     * @return bool
     */
    public function isAnAllergicIngredient(Ingredients $ingredients, User $user) :bool
    {
       return in_array($ingredients, $user->getAllergies()->toArray());
    }
}