<?php

namespace App\Form;

use App\Entity\Ingredients;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserAllergyType
 * @package App\Form
 * @author bernard SIMEON
 */
class UserAllergyType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('allergies', EntityType::class , [
                'label' => 'Select yours allergies',
                'class' => Ingredients::class,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => function (Ingredients $ingredients) {
                    return $ingredients->getName();
                }
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
