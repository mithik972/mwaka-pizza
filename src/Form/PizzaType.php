<?php

namespace App\Form;

use App\Entity\Ingredients;
use App\Entity\Pizza;
use App\Service\FileUploader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class PizzaType
 * @package App\Form
 * @author
 */
class PizzaType extends AbstractType
{
    private FileUploader $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('image',FileType::class, [
                'label' => 'Image (png/jpeg file) :',
                'mapped' => false,
                'required'   => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'Please upload a image file',
                    ])
                ],
            ])
            ->add('ingredients' , EntityType::class, [
                'label' => 'Ingredients list :',
                'class' => Ingredients::class,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => function (Ingredients $ingredients) {
                    return $ingredients->getName();
                }
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pizza::class,
        ]);
    }
}
