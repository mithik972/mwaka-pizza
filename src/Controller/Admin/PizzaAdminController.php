<?php

namespace App\Controller\Admin;

use App\Entity\Pizza;
use App\Form\PizzaType;
use App\Repository\PizzaRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PizzaAdminController
 * @package App\Controller\Admin
 * @author bernard SIMEON
 */
#[Route('/admin/pizza')]
class PizzaAdminController extends AbstractController
{
    /**
     * List pizzas admin side
     *
     * @param PizzaRepository $pizzaRepository
     *
     * @return Response
     */
    #[Route('/', name: 'app_pizza_index', methods: ['GET'])]
    public function index(PizzaRepository $pizzaRepository): Response
    {
        return $this->render('pizza/index.html.twig', [
            'pizzas' => $pizzaRepository->findAll(),
        ]);
    }

    /**
     * New pizza admin side
     *
     * @param Request $request
     * @param PizzaRepository $pizzaRepository
     * @param FileUploader $fileUploader
     *
     * @return Response
     * @throws \Exception
     */
    #[Route('/new', name: 'app_pizza_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PizzaRepository $pizzaRepository, FileUploader $fileUploader): Response
    {
        $pizza = new Pizza();
        $form = $this->createForm(PizzaType::class, $pizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pizza->calculatePrice();
            $data = $form->get('image')->getData();

            if ($data) {
                $pizza->setImage($fileUploader->upload($data));
            }

            $pizzaRepository->add($pizza, true);

            return $this->redirectToRoute('app_pizza_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pizza/new.html.twig', [
            'pizza' => $pizza,
            'form' => $form,
        ]);
    }

    /**
     * Show Pizza admin side
     *
     * @param Pizza $pizza
     *
     * @return Response
     */
    #[Route('/{id}', name: 'app_pizza_show', methods: ['GET'])]
    public function show(Pizza $pizza): Response
    {
        return $this->render('pizza/show.html.twig', [
            'pizza' => $pizza,
        ]);
    }

    /**
     * Edit Pizza admin side
     *
     * @param Request $request
     * @param Pizza $pizza
     * @param PizzaRepository $pizzaRepository
     * @param FileUploader $fileUploader
     *
     * @return Response
     * @throws \Exception
     */
    #[Route('/{id}/edit', name: 'app_pizza_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Pizza $pizza, PizzaRepository $pizzaRepository, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(PizzaType::class, $pizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pizza->calculatePrice();
            $data = $form->get('image')->getData();

            if ($data) {
                $pizza->setImage($fileUploader->replace($data, $pizza->getImage()));
            }

            $pizzaRepository->add($pizza, true);

            return $this->redirectToRoute('app_pizza_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pizza/edit.html.twig', [
            'pizza' => $pizza,
            'form' => $form,
        ]);
    }

    /**
     * Delete pizza admin side
     *
     * @param Request $request
     * @param Pizza $pizza
     * @param PizzaRepository $pizzaRepository
     *
     * @return Response
     */
    #[Route('/{id}', name: 'app_pizza_delete', methods: ['POST'])]
    public function delete(Request $request, Pizza $pizza, PizzaRepository $pizzaRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pizza->getId(), $request->request->get('_token'))) {
            $pizzaRepository->remove($pizza, true);
        }

        return $this->redirectToRoute('app_pizza_index', [], Response::HTTP_SEE_OTHER);
    }
}
