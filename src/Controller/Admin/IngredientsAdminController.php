<?php

namespace App\Controller\Admin;

use App\Entity\Ingredients;
use App\Form\IngredientsType;
use App\Repository\IngredientsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IngredientsAdminController
 * @package App\Controller\Admin
 * @author Bernard SIMEON
 */
#[Route('/admin/ingredients')]
class IngredientsAdminController extends AbstractController
{
    /**
     * List ingredients admin side
     *
     * @param IngredientsRepository $ingredientsRepository
     *
     * @return Response
     */
    #[Route('/', name: 'app_ingredients_index', methods: ['GET'])]
    public function index(IngredientsRepository $ingredientsRepository): Response
    {
        return $this->render('ingredients/index.html.twig', [
            'ingredients' => $ingredientsRepository->findAll(),
        ]);
    }

    /**
     * New ingredient admin side
     *
     * @param Request $request
     * @param IngredientsRepository $ingredientsRepository
     *
     * @return Response
     */
    #[Route('/new', name: 'app_ingredients_new', methods: ['GET', 'POST'])]
    public function new(Request $request, IngredientsRepository $ingredientsRepository): Response
    {
        $ingredient = new Ingredients();
        $form = $this->createForm(IngredientsType::class, $ingredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ingredientsRepository->add($ingredient, true);

            return $this->redirectToRoute('app_ingredients_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ingredients/new.html.twig', [
            'ingredient' => $ingredient,
            'form' => $form,
        ]);
    }

    /**
     * Show ingredients admin side
     *
     * @param Ingredients $ingredient
     *
     * @return Response
     */
    #[Route('/{id}', name: 'app_ingredients_show', methods: ['GET'])]
    public function show(Ingredients $ingredient): Response
    {
        return $this->render('ingredients/show.html.twig', [
            'ingredient' => $ingredient,
        ]);
    }

    /**
     * Edit ingredient admin side
     *
     * @param Request $request
     * @param Ingredients $ingredient
     * @param IngredientsRepository $ingredientsRepository
     *
     * @return Response
     */
    #[Route('/{id}/edit', name: 'app_ingredients_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Ingredients $ingredient, IngredientsRepository $ingredientsRepository): Response
    {
        $form = $this->createForm(IngredientsType::class, $ingredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ingredientsRepository->add($ingredient, true);

            return $this->redirectToRoute('app_ingredients_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ingredients/edit.html.twig', [
            'ingredient' => $ingredient,
            'form' => $form,
        ]);
    }

    /**
     * Delete Ingredient admin side
     *
     * @param Request $request
     * @param Ingredients $ingredient
     * @param IngredientsRepository $ingredientsRepository
     *
     * @return Response
     */
    #[Route('/{id}', name: 'app_ingredients_delete', methods: ['POST'])]
    public function delete(Request $request, Ingredients $ingredient, IngredientsRepository $ingredientsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ingredient->getId(), $request->request->get('_token'))) {
            $ingredientsRepository->remove($ingredient, true);
        }

        return $this->redirectToRoute('app_ingredients_index', [], Response::HTTP_SEE_OTHER);
    }
}
