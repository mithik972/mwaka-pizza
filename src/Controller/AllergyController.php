<?php

namespace App\Controller;

use App\Form\UserAllergyType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AllergyController
 * @package App\Controller
 * @author bernard SIMEON
 */
class AllergyController extends AbstractController
{
    /**
     * List and choice allergy client side
     *
     * @param Request $request
     * @param UserRepository $userRepository
     *
     * @return Response
     */
    #[Route('/allergy', name: 'app_allergy')]
    #[IsGranted('ROLE_USER')]
    public function index(Request $request, UserRepository $userRepository): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserAllergyType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->add($user, true);
            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('allergy/index.html.twig', [
         'form' => $form,
        ]);
    }
}
