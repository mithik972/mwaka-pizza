<?php

namespace App\Controller;

use App\Entity\Pizza;
use App\Repository\PizzaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 * @author bernard SIMEON
 */
#[Route('/our-pizzas')]
class HomeController extends AbstractController
{
    /**
     * List pizza client side
     *
     * @param PizzaRepository $pizzaRepository
     *
     * @return Response
     */
    #[Route('/', name: 'app_home')]
    public function index(PizzaRepository $pizzaRepository): Response
    {
        $pizzas = $pizzaRepository->findAll();

        return $this->render('home/index.html.twig', [
            'pizzas' => $pizzas,
        ]);
    }

    /**
     * Order pizza
     *
     * @param Pizza $pizza
     *
     * @return Response
     */
    #[Route('/order/{id}', name: 'app_pizza_order', methods: ['GET'])]
    public function order(Pizza $pizza): Response
    {
        return $this->render('home/show.html.twig', [
            'pizza' => $pizza,
        ]);
    }

    /**
     * Pizza in preparation client side
     *
     * @param Pizza $pizza
     *
     * @return Response
     */
    #[Route('/order/preparation/{id}', name: 'app_pizza_order', methods: ['GET'])]
    public function preparation(Pizza $pizza): Response
    {
        $order = uniqid($pizza->getName());

        return $this->render('home/show.html.twig', [
            'pizza' => $pizza,
            'order' => $order
        ]);
    }
}
