<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class FileUploader
 * @package App\Service
 * @author bernard SIMEON
 */
class FileUploader
{
    private $targetDirectory;

    /**
     * @var SluggerInterface
     */
    private SluggerInterface $slugger;


    /**
     * FileUploader constructor.
     *
     * @param $targetDirectory
     * @param SluggerInterface $slugger
     */
    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }


    /**
     * Upload file
     *
     * @param UploadedFile $file
     *
     * @return string
     * @throws \Exception
     */
    public function upload(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move( $this->targetDirectory, $fileName);
        } catch (FileException $e) {
            throw new \Exception('failed to laod media');
        }

        return $fileName;
    }

    /**
     * Replace old file and upload new file
     *
     * @param UploadedFile $newFile
     * @param string|null $oldFileName
     *
     * @return string
     *
     * @throws \Exception
     */
    public function  replace(UploadedFile $newFile, ?string $oldFileName): string
    {
        if ($oldFileName) {
            $this->delete($this->targetDirectory . '/' . $oldFileName);
        }

       return $this->upload($newFile);
    }

    /**
     * @param string $filename
     */
    public function delete(string $filename): void
    {
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * @return mixed
     */
    public function getTargetDirectory(): mixed
    {
        return $this->targetDirectory;
    }
}