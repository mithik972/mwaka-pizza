<?php

namespace App\DataFixtures;

use App\Entity\Ingredients;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 * @author Bernard SIMEON
 */
class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $hasher;

    /**
     * AppFixtures constructor.
     *
     * @param UserPasswordHasherInterface $hasher
     */
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) :void
    {
        $user = new User();
        $user->setEmail('admin@test.com');
        $user->setRoles(['ROLE_ADMIN']);
        $password = $this->hasher->hashPassword($user, 'azerty1234');
        $user->setPassword($password);
        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail('user@test.com');
        $user2->setRoles(['ROLE_USER']);
        $password = $this->hasher->hashPassword($user2, 'azerty1234');
        $user2->setPassword($password);
        $manager->persist($user2);

        foreach ($this->getIngredientsDetails() as $item) {
            $ingredient = new Ingredients();
            $ingredient->setName($item);
            $ingredient->setPrice( mt_rand( 50, 225 ) / 100);
            $ingredient->setCreatedAt(new \DateTimeImmutable());

            $manager->persist($ingredient);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    private function getIngredientsDetails() :array
    {
        return [
            'cheese',
            'oeg',
            'bacon',
            'minced meat',
            'potato',
            'minced chicken',
            'anchovy',
            'pineapple',
            'ham',
            'tomato sauce',
            'cream',
            'merguez',
            'corn',
            'olive',
            'sausage'
        ];
    }
}
