const image_input = document.querySelector("#pizza_image");

function imageHandler(e2) {
    document.querySelector("#image-view1").src =  e2.target.result;
}
if (image_input) {
    image_input.addEventListener("change", function() {
        const reader = new FileReader();
        if (this.files[0]) {
            reader.onload = imageHandler;
            reader.readAsDataURL(this.files[0]);
        } else {
            imageHandler({target:{result: ''}});
        }
    });
}
