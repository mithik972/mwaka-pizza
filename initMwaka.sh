# exit when any command fails
set -e

# echo an error message before exiting
trap 'echo "\"${last_command}\" command failed with exit code $?."' EXIT
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG

cd migrations/
rm * -rf
cd ..
composer install
symfony server:stop
symfony console  doctrine:database:drop --force

symfony console d:d:c
symfony console make:migration
symfony console d:m:m
symfony console doctrine:fixtures:load
php bin/phpunit
yarn install --ignore-engines
yarn run dev
symfony server:start -d